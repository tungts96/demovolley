package com.tungts.demovolley;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tungts.demovolley.Request.GsonRequest;
import com.tungts.demovolley.Request.MySingleton;
import com.tungts.demovolley.adapter.UserAdapter;
import com.tungts.demovolley.interfaces.OnLoadMoreListener;
import com.tungts.demovolley.interfaces.OnRecycleViewItemClick;
import com.tungts.demovolley.model.Page;
import com.tungts.demovolley.model.User;

import java.util.ArrayList;
import java.util.HashMap;

public class ListUserActivity extends AppCompatActivity {


    int total_page;
    int current_page = 1;

    RecyclerView rcvUser;
    UserAdapter userAdapter;
    ArrayList list;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);
        addControls();
        addEvents();
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        getListUserOnPage();
    }


    private void addEvents() {
        loadMore();
        recycleViewItemClick();
    }

    private void recycleViewItemClick() {
        userAdapter.setOnRecycleViewItemClick(new OnRecycleViewItemClick() {
            @Override
            public void itemClick(int positon) {
                User user = (User) list.get(positon);
                Intent intent = new Intent(ListUserActivity.this, DetailActivity.class);
                intent.putExtra("ST", user.getId());
                startActivity(intent);
            }
        });
    }

    private void loadMore() {
        userAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (current_page < total_page) {
                    Log.e("ST",current_page+"");
                    list.add(null);
                    userAdapter.notifyDataSetChanged();
                    current_page++;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getListUserOnPage();
                        }
                    }, 3000);
                }
            }
        });
    }

    private void addControls() {
        rcvUser = (RecyclerView) findViewById(R.id.rcvUser);
        list = new ArrayList();
        userAdapter = new UserAdapter(this, list, rcvUser);
        rcvUser.setLayoutManager(new LinearLayoutManager(this));
        rcvUser.setAdapter(userAdapter);
    }


    private void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void getListUserOnPage() {
        String url = getString(R.string.url_get_list_user_on_page, current_page);
        Log.e("ST",url);
        GsonRequest gsonRequest = new GsonRequest(Request.Method.GET, url, Page.class, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (current_page == 1){
                            Page page = (Page) response;
                            total_page = page.getTotal_pages();
                            Log.e("ST","Total" + total_page);
                            list.addAll(page.getData());
                            current_page++;
                            getListUserOnPage();
                        } else if (current_page == 2){
                            if (progressDialog.isShowing()){
                                progressDialog.cancel();
                            }
                            Page page = (Page) response;
                            list.addAll(page.getData());
                            userAdapter.notifyDataSetChanged();
                        } else {
                            list.remove(list.size()-1);
                            userAdapter.notifyDataSetChanged();
                            userAdapter.notifiDataLoadmore(((Page)response).getData());
                            userAdapter.setLoading();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showToast(error.getLocalizedMessage());
                    }
                });
        MySingleton.getInstance(this).addToRequestQueue(gsonRequest);
    }
}
