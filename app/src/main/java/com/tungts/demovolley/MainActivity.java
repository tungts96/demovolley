package com.tungts.demovolley;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tungts.demovolley.Request.MySingleton;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnGetUserById,btnGetListUserOnPage;

    EditText edtPostName, edtPostJob;
    Button btnPost;

    EditText edtPutName, edtPutJob;
    Button btnPut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControlls();
        addEvents();
    }

    private void addEvents() {
        btnGetUserById.setOnClickListener(this);
        btnGetListUserOnPage.setOnClickListener(this);
        btnPut.setOnClickListener(this);
        btnPost.setOnClickListener(this);
    }

    private void addControlls() {
        btnGetUserById = (Button) findViewById(R.id.btnGetUserById);
        btnGetListUserOnPage = (Button) findViewById(R.id.btnGetListUserOnPage);
        edtPostName = (EditText) findViewById(R.id.edtPostName);
        edtPostJob = (EditText) findViewById(R.id.edtPostJob);
        btnPost = (Button) findViewById(R.id.btnPost);
        edtPutName = (EditText) findViewById(R.id.edtPutName);
        edtPutJob = (EditText) findViewById(R.id.edtPutJob);
        btnPut = (Button) findViewById(R.id.btnPut);
    }

    private void postToAPI(final String namePost, final String jobPost) {
        String url = "https://reqres.in/api/users";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name",namePost);
                params.put("job",jobPost);
                return super.getParams();
            }
        };
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void putToAPI(final String namePut, final String jobPut) {
        String url = "https://reqres.in/api/users/2";
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name",namePut);
                params.put("job",jobPut);
                return super.getParams();
            }
        };
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btnGetUserById:
                Intent intent = new Intent(MainActivity.this, SingleUserActivity.class);
                startActivity(intent);
                break;
            case R.id.btnGetListUserOnPage:
                Intent intent1 = new Intent(MainActivity.this, ListUserActivity.class);
                startActivity(intent1);
                break;
            case R.id.btnPost:
                String namePost = edtPostName.getText().toString();
                String jobPost = edtPostJob.getText().toString();
                postToAPI(namePost,jobPost);
                break;
            case R.id.btnPut:
                String namePut = edtPutName.getText().toString();
                String jobPut = edtPutJob.getText().toString();
                putToAPI(namePut,jobPut);
                break;
        }
    }

}
