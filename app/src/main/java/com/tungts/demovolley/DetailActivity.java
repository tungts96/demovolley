package com.tungts.demovolley;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.tungts.demovolley.Request.GsonRequest;
import com.tungts.demovolley.Request.MySingleton;
import com.tungts.demovolley.model.Data;

public class DetailActivity extends AppCompatActivity {

    ImageView imgDetail;
    TextView tvId, tvFirstName, tvLastName;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imgDetail = (ImageView) findViewById(R.id.imgDetail);
        tvId = (TextView) findViewById(R.id.tvIdDetail);
        tvFirstName = (TextView) findViewById(R.id.tvFirstNameDetail);
        tvLastName = (TextView) findViewById(R.id.tvLastNameDetail);
        progressDialog = new ProgressDialog(this);

        int id = getIntent().getIntExtra("ST", 0);
        if (id != 0) {
            progressDialog.setMessage("Loading");
            progressDialog.show();
            //call API
            getUserById(id);
        }
    }

    private void getUserById(int idUser) {
        String url = getString(R.string.url_get_user_by_id, idUser);
        GsonRequest gsonRequest = new GsonRequest(Request.Method.GET, url, Data.class, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        if (progressDialog.isShowing()){
                            progressDialog.cancel();
                        }
                        Data data = (Data) response;
                        tvId.setText(getString(R.string.id,data.getData().getId()));
                        tvFirstName.setText(getString(R.string.first_name,data.getData().getFirst_name()));
                        tvLastName.setText(getString(R.string.last_name,data.getData().getLast_name()));
                        Picasso.with(DetailActivity.this).load(data.getData().getAvatar()).into(imgDetail);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailActivity.this, "Không có Id thỏa mãn", Toast.LENGTH_SHORT).show();
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(gsonRequest);
    }
}
