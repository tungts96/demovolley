package com.tungts.demovolley.model;

import java.util.ArrayList;

/**
 * Created by tungts on 7/24/2017.
 */

public class Page {

    private int total_pages;
    private ArrayList<User> data;

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<User> getData() {
        return data;
    }

    public void setData(ArrayList<User> data) {
        this.data = data;
    }

}
