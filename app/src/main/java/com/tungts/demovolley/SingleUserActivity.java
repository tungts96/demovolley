package com.tungts.demovolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.tungts.demovolley.Request.GsonRequest;
import com.tungts.demovolley.Request.MySingleton;
import com.tungts.demovolley.model.Data;

import org.json.JSONObject;

public class SingleUserActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtQueryId;
    Button btnSearch;

    TextView tvId, tvFirstName, tvLastName;
    ImageView imgProfileUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_user);
        addControls();
        addEvents();
    }

    private void addEvents() {
        btnSearch.setOnClickListener(this);
    }

    private void addControls() {
        edtQueryId = (EditText) findViewById(R.id.edtId);
        btnSearch = (Button) findViewById(R.id.btnGetUser);
        tvId = (TextView) findViewById(R.id.tvIdUser);
        tvFirstName = (TextView) findViewById(R.id.tvFirstNameUser);
        tvLastName = (TextView) findViewById(R.id.tvLastNameUser);
        imgProfileUser = (ImageView) findViewById(R.id.imgUser);
    }

    private void getUserById(int idUser) {
        String url = getString(R.string.url_get_user_by_id, idUser);
        GsonRequest gsonRequest = new GsonRequest(Request.Method.GET, url, Data.class, null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        Data data = (Data) response;
                        tvId.setText(getString(R.string.id,data.getData().getId()));
                        tvFirstName.setText(getString(R.string.first_name,data.getData().getFirst_name()));
                        tvLastName.setText(getString(R.string.last_name,data.getData().getLast_name()));
                        Picasso.with(SingleUserActivity.this).load(data.getData().getAvatar()).into(imgProfileUser);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SingleUserActivity.this, "Không có Id thỏa mãn", Toast.LENGTH_SHORT).show();
                    }
                });
        MySingleton.getInstance(this).addToRequestQueue(gsonRequest);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnGetUser) {
            int idUser = Integer.parseInt(edtQueryId.getText().toString());
            getUserById(idUser);
        }
    }

}
